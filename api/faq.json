[
	{
		"title": "Main",
		"questions": [
			{
				"question": "What is a Coin?",
				"answer": "Coin is a connected device that can hold and behave like the cards you already carry. Coin works with your debit cards, credit cards, gift cards, loyalty cards and membership cards. Instead of carrying several cards you carry one Coin. Multiple accounts and information all in one place."
			},
			{
				"question": "How do I get my cards onto a Coin?",
				"answer": "Our mobile app will allow you to add, manage and sync the cards that you choose to store on your Coin. The process of adding card information to the mobile app is very simple and is done by taking a picture or two and swiping your Coin through a small device we provide you with."
			},
			{
				"question": "How much does a Coin cost?",
				"answer": "Each Coin costs $100. For you early adopters there is a very limited quantity that can be purchased for $50."
			}
		]
	},
	{
		"title": "Form",
		"questions": [
			{
				"question": "Is a Coin really the same size as a typical credit card?",
				"answer": "Yes, really. The dimensions of a Coin are roughly 80.5 mm x 54 mm x 0.84 mm or 3.125\" x 2.125\" x 0.033\"."
			},
			{
				"question": "Do Coins only come in Midnight?",
				"answer": "Yes, but later down the road we will probably offer more colors."
			},
			{
				"question": "What material is a Coin made of?",
				"answer": "The skin of a Coin is made of plastic that is similar to that of a typical credit card."
			}
		]
	},
	{
		"title": "Function",
		"questions": [
			{
				"question": "How many cards can a Coin hold?",
				"answer": "The Coin mobile app can store an unlimited number of cards, however, a Coin can hold up to 8 cards at a time."
			},
			{
				"question": "What kinds of cards can I store in my Coin?",
				"answer": "Debit cards, credit cards, gift cards, loyalty cards and membership cards."
			},
			{
				"question": "What mobile platforms will Coin support?",
				"answer": "Coin mobile apps will be available for iOS and Android. We'll keep track of demand for Windows phone and BlackBerry and consider support for those platforms in the future."
			},
			{
				"question": "Can you pair a Coin to multiple mobile devices? For example, an iPhone and an iPad?",
				"answer": "You can only pair your Coin with a single device."
			},
			{
				"question": "Will the Coin work with an iPod?",
				"answer": "Coin will work with the latest generation iPod Touch. An Internet connection is required for certain operations like adding or syncing cards."
			},
			{
				"question": "Do you need an Internet connection to use Coin?",
				"answer": "Coin requires an Internet connection to add and sync cards. An Internet connection is not required to use Coin out in the world."
			},
			{
				"question": "Can I use my Coin for online payments?",
				"answer": "Coin is designed to allow you to use your cards out in the world. When you are making a purchase online, the Coin mobile app can be used to reference your card details. The Coin itself, however, cannot be used directly for online payments."
			},
			{
				"question": "Can I use a Coin in an ATM?",
				"answer": "Yes, a Coin can be used in an ATM."
			},
			{
				"question": "Can a Coin store my driver’s license or other forms of identification?",
				"answer": "Bottom line is that you should not rely on your Coin as a form of ID the next time you’re in line at the airport or at the club. A Coin can store an ID with a magnetic stripe, however, this will likely not suffice for you to use it out in the world. For example, a Coin cannot show your picture which is usually required with an ID."
			},
			{
				"question": "Can someone accidentally change which card is selected on my Coin?",
				"answer": "We’ve designed the button to toggle cards in a way that makes it difficult to trigger a \"press\" unintentionally. Dropping a Coin, holding a Coin, sitting on a Coin, or putting the Coin in a check presenter at a restaurant will not inadvertently toggle the card that is selected."
			},
			{
				"question": "Can someone intentionally change which card is selected on my Coin?",
				"answer": "Coin has an 'auto-lock' feature that works based on proximity. It allows the card to be swiped, but disables the button to change which card is selected."
			},
			{
				"question": "Do I need my phone with me to use Coin? How close does my Coin need to be to my phone to use it?",
				"answer": "No. Coin is a standalone device. There are, however, security features like notifying you that you forgot your Coin somewhere which you need to have your phone on you to take advantage of."
			},
			{
				"question": "Can I turn the Coin's Bluetooth radio on and off?",
				"answer": "No, Coin manages this for you."
			}
		]
	},
	{
		"title": "Security",
		"questions": [
			{
				"question": "Does Coin support chip & pin (EMV)?",
				"answer": "Coin is currently designed for the U.S. market and does not support chip & pin (EMV), however, future generations of the device will include EMV."
			},
			{
				"question": "How secure is Coin?",
				"answer": "Maintaining the integrity of your Coin’s data is critical to your peace of mind. That’s why our servers, mobile apps and the Coin itself use 128-bit or 256-bit encryption for all storage and communication (http and bluetooth). Additionally Coin can alert you in the event that you leave it somewhere, automatically disable certain device functions based on proximity to your phone, or fully deactivate itself based on the amount of time it's been away from your phone."
			},
			{
				"question": "What if I lose my Coin or someone steals it?",
				"answer": "In the event that your Coin loses contact with your phone for a period of time that you configure in the Coin mobile app, it will automatically deactivate itself."
			},
			{
				"question": "What if I lose my phone?",
				"answer": "When you pair your Coin with your phone, it is associated with your account and not that specific phone/device. If you lose your phone, it is possible to pair your Coin with your new phone after installing the Coin mobile app and signing in."
			},
			{
				"question": "What if both my phone and my Coin are lost or stolen?",
				"answer": "The fastest way to prevent problems in this scenario is to leverage the phone’s security features. Specifically using 'remote wipe' features to make your phone’s data inaccessible will help ensure that your Coin cannot be used as quickly as possible. We also strongly encourage iOS users to enable a passcode and turn on the 'Erase all data on this iPhone after 10 failed attempts' feature. If someone is able to get past the lock screen on your phone (or you don’t have it enabled), they will not be able to access card details stored on the phone, change settings, nor sync cards to your Coin via the Coin mobile app without signing in with your Coin account."
			},
			{
				"question": "How long can my Coin be out of contact with my phone before it deactivates itself?",
				"answer": "This will be a configurable setting in the Coin mobile app."
			},
			{
				"question": "What is the maximum range between Coin and my phone. What happens to Coin when it’s out of range?",
				"answer": "Coin can comfortably communicate with your phone within within a 7.5 m/25 ft range. When Coin is out of range of your phone, it will start a countdown to automatically deactivate itself if it does not re-establish communication with your phone within the period of time configured in the Coin mobile app."
			},
			{
				"question": "What’s the difference between 'auto-lock' and 'auto-deactivate'?",
				"answer": "The auto-lock feature will disable card selection on your Coin, effectively 'locking in' your card choice. Auto-deactivate is designed to completely shut down your Coin rendering it unusable until you take action."
			},
			{
				"question": "How does the 'auto-lock' feature work?",
				"answer": "The purpose of the auto-lock feature is to allow a Coin to remain swipeable once you have selected a card, but to prevent someone else from either accidentally or intentionally changing the card that is selected. If enabled, as soon as your Coin leaves your phone’s side, the feature will automatically trigger. Once your Coin and your phone are nearby one and other again, the button on the device will be re-enabled so that you can select a card."
			},
			{
				"question": "How does the 'auto-deactivate' feature work?",
				"answer": "The purpose of the auto-deactivate feature is to render your Coin useless in the event that it is lost or stolen. If your Coin is out of contact with your phone for a period of time that you configure in the Coin mobile app, it will automatically deactivate itself. When a Coin is deactivated, you will not be able to select which card to use nor swipe any of the cards stored on the device.If your Coin becomes deactivated due to your phone dying or being in airplane mode, there will be a way to unlock it temporarily. We will allow you to configure a sequence of button presses (a.k.a 'morse-code-like' passcode). If you correctly enter this button press sequence your Coin will be fully functional for a few minutes before it deactivates again.Once your phone and your Coin re-establish contact, your Coin can regain full functionality."
			},
			{
				"question": "Is Coin password protected?",
				"answer": "Your Coin account is password protected and the mobile app requires that you type in your password before you can access sensitive card details."
			},
			{
				"question": "Can I lock my Coin?",
				"answer": "Currently you cannot lock your Coin, but you don’t have to. Coin will automatically deactivate if it loses contact with your phone for a period of time that you configure in the Coin mobile app. In addition, Coin can disable functions like the button to select a card based on proximity to your phone."
			},
			{
				"question": "What if my phone runs out of power or is in airplane mode? Will my Coin be useable?",
				"answer": "Yes, but you may need to unlock it if the Coin becomes deactivated due to being out of contact with your phone for too long."
			},
			{
				"question": "What’s involved in adding a card to the Coin mobile app?",
				"answer": "Adding a card to the Coin mobile app requires a few steps. The app will guide you through the process: 1) Take a picture of the front of the card, 2) take a picture of the back of the card, 3) swipe the card through a reader that we provide (which you plug into the headphone jack of your phone), and 4) verify that you own the card.Please also note that when you register a Coin account, we will require you to provide information that establishes your identity."
			},
			{
				"question": "What steps does the Coin app take to prevent cards from being added fraudulently?",
				"answer": "The Coin app requires that you take a picture of the front and back of the card, type in card details, and then swipe the card (using a reader we provide) to ensure the card’s encoded magnetic stripe data matches the card details provided. It is not possible to complete these steps unless you are in physical possession of a card. As an additional safeguard, the Coin app will only allow you to add cards you own."
			},
			{
				"question": "Can a Coin be used to skim cards?",
				"answer": "No. You can only add cards that you own to your Coin."
			},
			{
				"question": "Can card data be skimmed from a Coin?",
				"answer": "A Coin is less susceptible to some card skimming techniques that take a picture of the card as it is swiped since Coin does not display the full card details on the front or back of the device. A Coin is no less susceptible than your current cards to other forms of skimming that capture data encoded in the magnetic stripe as the card is swiped."
			},
			{
				"question": "Does Coin satisfy PCI DSS standards for storing and transmitting card data?",
				"answer": "Coin is in the process of earning a PCI DSS certification."
			},
			{
				"question": "Do you store card details on your servers?",
				"answer": "When you purchase a Coin, your payment details are stored on PCI-compliant infrastructure. When you use Coin, we will only store non-sensitive card details on our servers."
			},
			{
				"question": "Does Coin have a PCI PA-DSS validation?",
				"answer": "The PCI Security Standards Council PA-DSS program addresses payment applications used to accept and process payment for goods and services. A device such as a Coin is seen as similar to a payment card in a consumer’s wallet and the standard does not apply."
			}
		]
	},
	{
		"title": "Durability",
		"questions": [
			{
				"question": "How long does a Coin last? Do I recharge it? What happens when my Coin’s battery dies?",
				"answer": "Coins are designed to last for 2 years under normal usage and do not need to be recharged. Once the battery dies you will need to replace your Coin."
			},
			{
				"question": "How do you figure that a Coin will last 2 years? What constitutes normal usage?",
				"answer": "Here’s how we modeled typical usage. On a daily basis, we assume you’ll toggle which card you want to use and swipe your Coin 10-20 times a day. In addition, you may adjust which cards you want to store on your Coin a few times a week. Initially when you’re getting to know your Coin you will likely be syncing often. Over time we find that most people sync much less frequently once his or her cards are on a Coin. If you swipe and sync more frequently on a consistent basis, your Coin’s battery will drain more quickly."
			},
			{
				"question": "If I use the device 2-3 times per day compared to the typical usage model of 10-20, would this extend the battery life? If yes, by how much?",
				"answer": "If you use your Coin less frequently it will indeed help extend battery life. If you only use it a few times a day, we estimate this will add on a month or two of battery life."
			},
			{
				"question": "Won’t the Bluetooth connection between my Coin and my phone drain the Coin’s battery?",
				"answer": "Bluetooth Low Energy (BLE) is designed to consume very little power and in addition to that we’ve carefully optimized communication between the Coin and Coin mobile app. Under normal usage, a Coin’s battery should last 2 years."
			},
			{
				"question": "When does a Coin start using its battery?",
				"answer": "Coin starts using its battery as soon as it is manufactured. We have carefully optimized how it uses power before it is ever synced with your phone and after it is synced with your phone to maximize the lifetime of the device."
			},
			{
				"question": "Is Coin waterproof?",
				"answer": "Coins are water resistant but not waterproof. If you spill your coffee or juice box on your Coin it's probably no big deal. If you put it through the washer or take it swimming your Coin will likely cease to function as designed."
			},
			{
				"question": "My soufflés keep collapsing! What can I do?",
				"answer": "In order for the meringue to peak properly we suggest adding a little lemon juice to the béchamel. This strengthens the mixture and prevents tragedy."
			},
			{
				"question": "Can magnets break my Coin?",
				"answer": "Normal magnetic exposure (wallets with money clips that use magnets) won’t harm your Coin. If your job or hobby puts you in close proximity to above average strength electro-magnets this will most likely render your Coin inoperable."
			},
			{
				"question": "Will a Coin be damaged by going through a metal detector or an x-ray machine in an airport?",
				"answer": "A Coin will not be damaged going through a standard metal detector or x-ray machine."
			},
			{
				"question": "What happens if I sit on my Coin?",
				"answer": "Coins are built with some durability and flexibility so don’t worry too much. Your Coin however is an electronic device so if it is folded or bent at extreme angles it will most likely break."
			},
			{
				"question": "What happens when the magnetic stripe wears out?",
				"answer": "If the magnetic stripe becomes unreadable you will need to replace your Coin. Since the electronics that generate the stripe data are below the card surface this should be much less of an issue than with a traditional card where the stripe actually comes in contact with the card reader."
			}
		]
	},
	{
		"title": "Referral Program",
		"questions": [
			{
				"question": "How do referrals work?",
				"answer": "When you pre-order your Coin we conjure up a unique referral code URL using a mystical form of alchemy. If someone clicks the referral code link and pre-orders a Coin within 90 days, you get $5 credit. Magic loves to be shared."
			},
			{
				"question": "What are the rules?",
				"answer": "We want this to work in your favor but there are a few rules. This isn’t Thunder Dome.First, we’ve got to be able to charge the person you refer. If there are any hiccups in processing the pre-order payment for a referral we cannot bestow the $5 credit upon you.Second, regardless of the quantity of Coins your friend orders you will still only receive a single $5 dollar credit.Third, we will honor your referral credits up to 90 days after you pre-order your Coin.Finally, you can earn up to $50 in referral credit. For you early adopters this means that if you get 10 of your peeps to pre-order a Coin, your Coin is on us."
			},
			{
				"question": "How will I know when I’ve earned referral credits?",
				"answer": "Each time you earn a $5 referral credit, we will send you an email."
			},
			{
				"question": "How will I receive my referral credit?",
				"answer": "We will issue a $5 refund to the account you used to purchase your Coin. If we hit any snags issuing the refund we’ll contact you via email and get it straightened out. We take the referral process seriously and want to reward you for spreading the word about Coin."
			},
			{
				"question": "Can I use my own referral code?",
				"answer": "Clever, but no."
			},
			{
				"question": "Will the referrer be notified when I use his code?",
				"answer": "When a referral is successfully processed the referrer is notified that…someone…has used his/her referral link. However, the identity of The Masked Referrer is forever a mystery! Unless, of course, you tell them it was you."
			}
		]
	}
]
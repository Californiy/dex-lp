module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options: {
				separator: ';'
			},
			dist: {
				src: [
					'js/vendor/angular/angular.js',
					'js/vendor/angular/angular-route.js',
					'js/vendor/angular/angular-animate.js',
					'js/vendor/angular/angular-sanitize.js',
					'js/vendor/underscore.js',

					'js/vendor/angular-lib/ocModal.js',
					'js/vendor/angular-lib/ui-utils.js',

					'js/app/controller/body-ctrl.js',

					'js/app/controller/index-screen.js',
					'js/app/controller/about-screen.js',
					'js/app/controller/faq-screen.js',
					'js/app/controller/rates-screen.js',
					'js/app/controller/contacts-screen.js',

					'js/app/app.js'

//					'js/app/directive/modal-directive.js'
				],
				dest: 'build/<%= pkg.name %>.js'
			}
		},
		watch: {
			js: {
				files: ['js/app/**/*.js'],
				tasks: ['concat']
			},
			css: {
				files: ['style/*.less'],
				tasks: ['less']
			}
		},
		less: {
			dev: {
				files: {
					"build/<%= pkg.name %>.css": "style/init.less"
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.registerTask('default', 'watch');
};
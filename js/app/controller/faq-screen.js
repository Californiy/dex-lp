function FaqScreen($rootScope, $scope, $http) {
	$rootScope.bodyClass = 'faq-page';

	// ! показать спиннер
	$http.get('/api/faq.json').success(function (data) {
		// ! убрать спиннер

		data[0].opened = true;
		$scope.groups = data;
	});

	$scope.click = function (group) {
		// не обрабатываем клик, если поиск активен
		if (!(typeof $scope.search_query === 'undefined' || $scope.search_query === "")) return;

		group.opened = !group.opened;
	}

	$scope.$watch('search_query', function (newValue, oldValue) {
		var isStart = (newValue !== "" && typeof newValue !== 'undefined') && (oldValue === "" || typeof oldValue === 'undefined'),
				isFinish = newValue === "";

		// запоминаем состояние до поиска
		if(isStart) {
			_($scope.groups).each(function(group){
				group.lastOpened = Boolean(group.opened);
			});
		}

		// раскрываем группы во время поиска
		(function (reg) {
			// TODO: найти нативную реализацию фильтра, а лучше закешированный результат
			_($scope.groups).each(function (group) {
				group.opened = _(group.questions).some(function (q) {
					return q.answer.search(reg) !== -1 || q.question.search(reg) !== -1;
				});
			});
		})(new RegExp(newValue, 'gi'));

		// возвращаем состояние до поиска
		if(isFinish) {
			_($scope.groups).each(function(group){
				group.opened = group.lastOpened;
				delete group.lastOpened;
			});
		}
	});
}
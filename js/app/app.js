(function (ng) {
	var app = ng.module('landing', ['ngRoute', 'ngSanitize', 'oc.modal', 'ui.highlight'])

	app.config(function ($routeProvider) {
		$routeProvider
				.when('/', {templateUrl: 'screen/index.html', controller: 'IndexScreen'})
				.when('/contacts', {templateUrl: 'screen/contacts.html', controller: 'ContactsScreen'})
				.when('/about', {templateUrl: 'screen/about.html', controller: 'AboutScreen'})
				.when('/rates', {templateUrl: 'screen/rates.html', controller: 'RatesScreen'})
				.when('/faq', {templateUrl: 'screen/faq.html', controller: 'FaqScreen'})
				.otherwise({redirectTo: '/404', templateUrl: 'screen/404.html'});
	});

})(angular);